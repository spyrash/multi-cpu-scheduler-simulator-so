#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(int argc,char **argv){
	FILE *fp;
	int num_proc;
	int num_event;
	int var_rand;
	srand(time(NULL));
	if(argc < 3){
	printf("\n digitare il numero di processi e il numero di eventi entrambi minore di 10 \n e maggiori 0 in ques'ordine \n ./spawn_process num_proc num_event \n");
	return 0;
	}
	num_proc=atoi(argv[1]);
	num_event=atoi(argv[2]);
	if(num_proc==0 || num_event==0){
		printf("digitare entrambi i numeri in formato intero >0 ( no stringhe,lettere,caratteri ecc..) \n");
		return 0;
	}
	if(atoi(argv[1]) > 10 || atoi(argv[2])>10){
		printf("valori troppo alti o sbagliati, immettero un intero minore di 10 \n");
		return 0;
	}
    // fine controlli ho 2 interi
      
      
      for(int i=0; i< num_proc ; i++){
		 
		  char s[8];
		  sprintf(s,"p%d.txt",i+1);
		  fp=fopen(s,"w");
		  if(fp == NULL){
			  printf("errore apertura file \n");
			  return 0; 
		  }
		  fprintf(fp,"PROCESS  %d %d \n",i+1,0);
		 for(int j=0; j< num_event;j++){
			  var_rand = rand() %41  +1;
			  if(j%2 ==0){
				  fprintf(fp,"CPU_BURST   %d \n",var_rand);
				}
			else{
				fprintf(fp,"IO_BURST    %d \n",var_rand);
			}
	    }
	    fclose(fp);
	}  
		  
} 
    
